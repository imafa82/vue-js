import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from "./routes";
import VueTouch from 'vue-touch';

Vue.use(VueRouter);
Vue.use(VueTouch);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
