import HelloWorld from "../components/HelloWorld";
import Page from "../components/Page";
import User from "../components/User";
import Users from "../components/Users";
import Actors from "../components/Actors";
import ActorList from "../components/actors/ActorList";
import ActorShow from "../components/actors/ActorShow";
import ActorEdit from "../components/actors/ActorEdit";

const routes =[
        {
            path: '/',
            name: 'HelloWorld',
            component: HelloWorld,
            menuTitle: 'Home Page'
        },
        {
            path: '/page',
            name: 'Page',
            component: Page,
            menuTitle: 'Pagina'
        },
        {
            path: '/actors',
            name: 'Actors',
            component: Actors,
            menuTitle: 'Attori',
            children: [
                {
                    path: '', component: ActorList, name: 'ActorList'
                },
                {
                    path: ':id', component: ActorShow, name: 'ActorShow'
                },
                {
                    path: ':id/edit', component: ActorEdit, name: 'ActorEdit'
                }
            ]
        },
        {
            path: '/users',
            name: 'Users',
            component: Users,
            menuTitle: 'Utenti'
        },
        {
            path: '/user/:id',
            name: 'User',
            component: User
        },
        {
         path: '*',
         redirect: {name: 'HelloWorld'}
        }
];
export default routes;
