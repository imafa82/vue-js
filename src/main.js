import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Card from "./components/Card";
import Header from "./components/Header";
import VueResource from 'vue-resource';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all';

Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.http.options.root = 'https://jsonplaceholder.typicode.com/';
Vue.http.options.header = {
  'Content-type': 'application/json'
};
Vue.http.interceptors.push((request, next) => {
  if(localStorage.getItem('token')){
    request.headers.set('Authorization', 'Bearer ' +localStorage.getItem('token'));
  }
  next(response => {
    if(response.status === 401){
      localStorage.removeItem('token');
      router.go();
    }
  });
});
Vue.component('app-card', Card);
Vue.component('app-header', Header);
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
